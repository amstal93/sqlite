FROM registry.gitlab.com/jitesoft/dockerfiles/alpine:latest
ARG SQLITE_VERSION
ARG SQLITE_VERSION_FULL
LABEL maintainer="Johannes Tegnér <johannes@jitesoft.com>" \
      maintainer.org="Jitesoft" \
      maintainer.org.uri="https://jitesoft.com" \
      com.jitesoft.project.repo.type="git" \
      com.jitesoft.project.repo.uri="https://gitlab.com/jitesoft/dockerfiles/sqlite" \
      com.jitesoft.project.repo.issues="https://gitlab.com/jitesoft/dockerfiles/sqlite/issues" \
      com.jitesoft.project.registry.uri="registry.gitlab.com/jitesoft/dockerfiles/sqlite" \
      com.jitesoft.app.sqlite.version="${SQLITE_VERSION}" \
      com.jitesoft.app.sqlite.version.full="${SQLITE_VERSION_FULL}"

RUN apk add --no-cache sqlite

ENTRYPOINT ["sqlite3"]
